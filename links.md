# Git und Markdown Referenzen

Hier sind einige nützliche Links zu Git- und Markdown-Referenzen:

- [Git Dokumentation](https://git-scm.com/doc)
- [GitLab Hilfe](https://docs.gitlab.com/)
- [Gitlab Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
# Branches in Git

Branches in Git ermöglichen es, verschiedene Entwicklungszweige unabhängig voneinander zu verwalten. Sie sind nützlich, um neue Funktionen zu entwickeln, Fehler zu beheben oder Experimente durchzuführen, ohne den Hauptcode zu beeinträchtigen.

## Erstellen eines neuen Branches

Um einen neuen Branch zu erstellen, verwenden Sie den Befehl:

```bash
git branch branchname
```
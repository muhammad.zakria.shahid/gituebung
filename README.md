# GitUebung

## Was ist Git?

Git ist ein verteiltes Versionskontrollsystem, das von Linus Torvalds im Jahr 2005 erfunden wurde. Es ermöglicht Entwicklern, Änderungen am Quellcode zu verfolgen, Zusammenarbeit zu erleichtern und verschiedene Versionen eines Projekts zu verwalten.

## Was kann man mit Git machen?

- Verfolgung von Änderungen am Quellcode.
- Zusammenarbeit mit anderen Entwicklern.
- Verwaltung von verschiedenen Versionen eines Projekts.
- Branching und Merging für parallele Entwicklung.
- Rückgängigmachen von Änderungen mit Hilfe von Commits.


```bash
# Eine Datei dem Staging-Bereich hinzufügen
git add <filename>

# Einen Commit erstellen
git commit -m "Commit-Nachricht"

# Den Status des Repositories anzeigen
git status

# Änderungen vom Remote-Repository abrufen
git pull

# Lokale Änderungen zum Remote-Repository hochladen
git push
```

[Branches](branches.md)
[Branches erklärt in 1 Minute](https://www.youtube.com/watch?v=YyFrdoD-Wjk)

[Links zu Git- und Markdown-Referenzen](links.md)